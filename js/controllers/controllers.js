﻿app.controller('InformationController', function ($scope) {

  $scope.go501 = function() {
    window.open('http://501creative.com', '_system' );
  }
});
app.controller('SendMailCtrl', function ($scope, pictabuzzService, Configs) {
  $scope.baseEmailAddress = Configs.baseEmailAddress;
  $scope.sendConfirms = function() {
    pictabuzzService.sendMail('needs-confirm', function(result) {
      if (result) {
        alertify.success('Emails have been sent');
      }
      else {
        alertify.error('Something has gone wrong. Contact an administrator');
      }
    });
  }
  $scope.sendPhotos = function() {
    pictabuzzService.sendMail('needs-photo', function(result) {
      if (result) {
        alertify.success('Emails have been sent');
      }
      else {
        alertify.error('Something has gone wrong. Contact an administrator');
      }
    });
  }

});
app.controller('SendNotificationCtrl', function ($scope, pictabuzzService) {
  $scope.submit = function() {
    var data = {
      message: $scope.message,
    };
    pictabuzzService.sendNotification(data, function(result) {
      if (result) {
        alert('Notifications sent.');
      }
      else {
        alert('Something has gone wrong. Contact an administrator');
      }
    });
  }
});
app.controller('MakeCallsCtrl', function ($scope, pictabuzzService) {
  $scope.submit = function() {
    var data = {
      stringTime: $scope.stringTime,
      message: $scope.message,
      to: $scope.to,
      from: $scope.from
    };
    pictabuzzService.scheduleCalls(data, function(result) {
      if (result) {
        alert('Calls have been schedule. If you need to cancel or confirm this call, contact an administrator.');
      }
      else {
        alert('Something has gone wrong. Contact an administrator');
      }
    });
  }
});

app.controller('LogController', function ($scope, reports, pictabuzzService) {
  function timeConverter(timestamp) {
    var date = new Date(timestamp);
    var month = date.getMonth();
    var day = date.getDay();
    var year = date.getFullYear();

    var formattedTime = month + '/' + day + '/' + year;
    return formattedTime;

  }
  var result = reports;
  $scope.logs = result;
  var dateLabels = _.keys(result.visits);
  var dateValues = _.values(result.visits);
  var uniqueValues = [];
  _.each(result.uniqueVisits, function(obj, label) {
    uniqueValues.push(obj.count);  
  });
  $scope.visitSum = _.reduce(dateValues, function(memo, num){ return memo + num; }, 0);
  $scope.uniqueSum = _.reduce(uniqueValues, function(memo, num){ return memo + num; }, 0);
  $scope.visits = {
    labels : dateLabels,
    datasets : [
      {
        fillColor : "rgba(151,187,205,1",
          strokeColor : "rgba(151,187,205,1)",
          pointColor : "rgba(151,187,205,1)",
          pointStrokeColor : "#6F8295",
          data : dateValues,
      },
      {
        fillColor : "rgb(244, 188, 24)",
        strokeColor : "rgb(244, 188, 24)",
        pointColor : "rgb(244, 188, 24)",
        pointStrokeColor : "#fff",
        data : uniqueValues,
      }
    ]
  };
  $scope.chart = {
    labels : ["Confirmed Users", "Mobile Users", "Users with Photos", "Total Parents"],
    datasets : [
      {
        fillColor : "rgba(151,187,205,1)",
        strokeColor : "#6F8295",
        pointColor : "rgba(151,187,205,0)",
        pointStrokeColor : "#e67e22",
        data : [result.confirmedUsers, result.mobileUsers, result.usersWithPhotos, result.totalParents]
      }
    ], 
  };
  $scope.confirmed = [
    {
      value: result.confirmedUsers,
      color:"#F38630"
    },
    {
      value : result.totalParents,
      color : "#E0E4CC"
    },
  ];
  $scope.mobile = [
    {
      value: result.mobileUsers,
      color:"#F38630"
    },
    {
      value : result.totalParents,
      color : "#E0E4CC"
    },
  ];
  $scope.photos = [
    {
      value: result.usersWithPhotos,
      color:"#F38630"
    },
    {
      value : result.totalParents,
      color : "#E0E4CC"
    },
  ];
  //  pictabuzzService.getNames(function(names) {
    //    pictabuzzService.getLogs(function(result) {
      //      var logs = {};
      //      for (var i in result) {
        //        result[i].timestamp = timeConverter(result[i].timestamp);
        //        var nameId = result[i].data;
        //        delete result[i].data;
        //        if (logs[names[nameId]] === undefined) {
          //          logs[names[nameId]] = [result[i]];
          //        }
          //        else {
            //          logs[names[nameId]].push(result[i]);
            //        }
            //      }
            //      var node = prettyPrint(logs);
            //      $('h2').append(node);
            //    });
            //  });
});
app.controller('ConfirmAccountController', function ($scope, $routeParams, $rootScope, $timeout, pictabuzzService, Configs) {
  var user_hash = $routeParams.userHash;
  $scope.submit = function() {
    pictabuzzService.confirmAccount({
      username: $scope.username,
      password: $scope.password,
      user_hash: user_hash
    }, function(response) {
      if (response != 'false') {
        // login
        $.cookie('pictabuzz_firebase_token', response);
        pictabuzzService.loginUser(undefined, function(result){
          $rootScope.user = result;
          $rootScope.role = result.auth.role;
          $timeout(function(){
            alertify.success('Your account has been confirmed.');
            alertify.log('Please upload a photo.');
            window.location = '/#/person/' + result.auth.id;
          },0);
        });
      }
      else {
        alert('You have chosen a username that is already taken. Try again, or contact ' + Configs.clientShortName + ' to receive your login and password.');
      }
    });
  }
});

app.controller('HomeController', function ($scope, Configs) {
  $scope.clientShortName = Configs.clientShortName;
});

app.controller('PanelCtrl', function ($scope, pictabuzzService, $timeout, $rootScope, $navigate) {
  $scope.filter_type = [];
  $scope.filter_type[0] = {'background-color': '#333333'};//, 'color': '#F1E7B4'};
  $scope.filter_group_type = [];
  $scope.filter_group_type[0] = {'background-color': '#333333'};//, 'color': '#F1E7B4'};
  pictabuzzService.getPersonTypes(function(ss){
    var types = ss.val();
    $rootScope.tax.person_types = types;
    $scope.person_types = $rootScope.tax.person_types;
    $scope.person_types[0] = 'All Person Types';
  });
  $scope.groups = [];
  pictabuzzService.getClasses(function(ss){
    var groups = ss.val();
    $rootScope.groups = groups;
    for (var i in groups) {
      var group = groups[i];
      $scope.groups[group.order] = {
        name: group.name,
        id: i
      }
    };
    $scope.groups[0] = {
      name: 'All Groups',
      id: '*'
    }
  });
  $rootScope.query.groups = "*";
  $scope.choose_type = function(type, sent_from_group) {
    $rootScope.query.person_type = type;
    if (sent_from_group) {
      for (var i in $scope.person_types) {
        if (i != type) {
          $scope.filter_type[i] = {'background-color': 'auto'};//, 'color': 'white'};
        }
        else {
          $scope.filter_type[i] = {'background-color': '#333333'};//, 'color': '#F1E7B4'};
        }
      }
    }
    else {
      for (var i in $scope.person_types) {
        if (i != type) {
          $scope.filter_type[i] = {'background-color': 'auto'};//, 'color': 'white'};
        }
        else {
          $scope.filter_type[i] = {'background-color': '#333333'};//, 'color': '#F1E7B4'};
        }
      }
      $scope.choose_group('*', true);
    }
  }
  $rootScope.choose_type = $scope.choose_type;
  $scope.choose_group = function(type, sent_from_type) {
    $('.mb-page .content').scrollTop(0);   
    if (type != '*') {    
      $rootScope.query.name = '';   
    }
    $navigate.go('/manage-users');
    $rootScope.query.groups = type;
    if (sent_from_type) type = '*';
    for (var i in $scope.groups) {
      if ($scope.groups[i].id != type) {
        $scope.filter_group_type[i] = {'background-color': 'auto'};//, 'color': 'white'};
      }
      else {
        $scope.filter_group_type[i] = {'background-color': '#333333'};//, 'color': '#F1E7B4'};
      }
    }
    if (!sent_from_type) {
      $scope.choose_type(0, true);
    }
    $rootScope.snapper.close();		
  }
  $rootScope.choose_group = $scope.choose_group;

  $scope.searchMade = function(){
    if ($rootScope.query.name != '') {
      $scope.choose_group('*');
    }
  }


});

app.controller('HomeControllerMobile', function ($scope, pictabuzzService, $timeout, $rootScope, $navigate, Configs) {
  $timeout(function() {
    navigator.splashscreen.hide();
  }, 1000);
  console.log('Loading HomeControllerMobile Controller');
  var local_username = window.localStorage.getItem("pictabuzz_userid");
  if (local_username != '' && local_username != undefined) {
    $scope.username = local_username;
  }
  $scope.loginUser = function() {
    $rootScope.startLoader();
    var data = {
      username: $scope.username,
      password: $scope.password,
    }
    pictabuzzService.loginUser( data, function( result ) {
      if (result.error == undefined) {
        $rootScope.user = result;
        $rootScope.role = result.auth.role;
        $timeout(function(){
          $navigate.go('/manage-users');
        },0);

        // window.location = '/shim.html#/person/' + result.auth.id;
      }
      else {
      }
    },
    function( message, level ) {
    }
    );
  }
  $scope.goReset = function() {
    window.open('http://' + Configs.subdomain + '.pictabuzz.com/#/password-reset-request', '_blank' );
  }

});

app.controller('ManageAccountController', function ($timeout, $scope, $rootScope, pictabuzzService, $navigate, $q, $http, Configs ) {
  checkUser();
  function checkUser() {
    if ($rootScope.groups == undefined) {
      pictabuzzService.getClasses(function(ss){
        var groups = ss.val();
        $scope.rootgroup = $rootScope.groups = groups;
      });
    }
    $scope.rootgroup = $rootScope.groups;
    if ($rootScope.user == undefined) {
      $timeout(function(){
        checkUser();
      },500);

    }
    else {
      $scope.relations = {};
      pictabuzzService.getPerson($rootScope.user.auth.id, function(person){
        $('#mobile-drawers').show();
        $scope.person = person.val();
        var id = person.name();
        if ($scope.person.array_fields != undefined) {
          if ($scope.person.array_fields.relationships != undefined) {
            for (var relation_id in $scope.person.array_fields.relationships) {
              pictabuzzService.getPerson(relation_id, function(ss) {
                $scope.relations[ss.name()] = ss.val();
              });
            }
          }
        }
        // $scope.person = person.val();
        if ($scope.person.basic_fields.photo_file == undefined) {
          $scope.person.basic_fields.photo_file = Configs.baseFileUrl + Configs.photoLarge;
        }
        $scope.image_url = Configs.baseFileUrl + $scope.person.basic_fields.photo_file;
        $scope.snapper = $rootScope.snapper;
        $scope.panel = function() {
          $('.mobile-list').scrollTop(0);
          if( $rootScope.snapper.state().state=="right" ){
            $rootScope.snapper.close();

          } else {
            $rootScope.snapper.expand('right');
          }
        }

        $scope.sendForm = function(){
        }

        $scope.editInformation = function() {
          $rootScope.startLoader();
          if ($scope.editing != true) {
            $scope.editing = true;
            $('.contact-info p').hide();
            // $('.contact-info input, .contact-info i').show().blur();
            $('.content.panel').scrollTop($('.contact-info').offset().top ,0 ); 
            $timeout(function(){
              $('.contact-info input, .contact-info i').show();
              $('.contact-info input:first').focus();
              $rootScope.stopLoader();
            },500);
            $('.edit-information div').html('Save My Information');
          }
          else {
            $scope.editing = false;
            $('.contact-info input, .contact-info i').hide();
            $('.contact-info p').show();
            pictabuzzService.savePerson(id, $scope.person, function() {
              $rootScope.stopLoader();
              $('.edit-information div').html('Edit My Information');
            });
          }
        }

        $scope.uploadPhoto = function() {
          var promises = [$q.defer(), $q.defer()];
          var promise = $q.all([promises[0].promise, promises[1].promise]);
          promise.then(function(){
            pictabuzzService.savePerson(id, $scope.person, function() {
              $rootScope.stopLoader();
              $timeout(function() {
                $scope.image_url = 'https://pictabuzz.s3.amazonaws.com/' + $scope.person.basic_fields.photo_file;
              },0);
             // if ($.browser.mobile != true) {
             //   alertify.success('Person has been saved.');
             // }
            }, function(error){
            });
          });
          if (Configs.isIphone) {
            console.log('calling iPhone filepicker');
            var filepicker = window.plugins.filepicker;
            filepicker.pick(
              {
                dataType: '*/*',
                sourceNames: ['FPSourceCameraRoll'],
              },
              function(FPFile){
                for (var i in FPFile) {
                }
                $scope.$apply(function() {
                  if (FPFile.FPPickerControllerRemoteURL != undefined && FPFile.FPPickerControllerRemoteURL != null) {
                    $rootScope.startLoader();
                    pictabuzzService.uploadImage({data: FPFile}, function(result) {
                      var new_photo_file = JSON.parse(result).key;
                      $scope.person.basic_fields.photo_file = new_photo_file;
                      $scope.person.basic_fields.photo_file_thumb = new_photo_file;
                      $scope.image_url = 'https://pictabuzz.s3.amazonaws.com/' + $scope.person.basic_fields.photo_file;
                      pictabuzzService.savePerson(id, $scope.person, function() {
                        $rootScope.stopLoader();
                      });
                    });
                  }
                });
                // filepicker.convert(FPFile, {width: 400, height: 400, fit: 'crop'}, {location: "S3", access: "public"},
                //   function(new_FPFile){
            				// $timeout(function() {
            				// 	$scope.person.basic_fields.photo_file = new_FPFile.key;
            				// 	promises[0].resolve();
    				        // },0);
                //   }
                // );
                // filepicker.convert(FPFile, {width: 100, height: 100, fit: 'crop'}, {location: "S3", access: "public"},
                //   function(new_FPFile){
            				// $timeout(function() {
            				// 	$scope.person.basic_fields.photo_file_thumb = new_FPFile.key;
            				// 	promises[1].resolve();
            				// },0);
                //   }
                // );
              },
              function(FPError){
              }
            );
          } else {
            console.log('calling Android filepiceker');
            window.androidPhoto = function() {
              cordova.exec( function(d) {
                console.log('FPSuccess : ' + d);
                var FPFile = {};
                FPFile.FPPickerControllerRemoteURL = d;
                $scope.$apply(function() {
                console.log(FPFile.FPPickerControllerRemoteURL);
                if (FPFile.FPPickerControllerRemoteURL !== undefined && FPFile.FPPickerControllerRemoteURL !== null) {
                  $rootScope.startLoader();
                  pictabuzzService.uploadImage({data: FPFile}, function(result) {
                    console.log('Successful upload');
                    console.log(JSON.parse(result).key);
                    var new_photo_file = JSON.parse(result).key;
                    $scope.person.basic_fields.photo_file = new_photo_file;
                    $scope.person.basic_fields.photo_file_thumb = new_photo_file;
                    $scope.image_url = 'https://pictabuzz.s3.amazonaws.com/' + $scope.person.basic_fields.photo_file;
                    pictabuzzService.savePerson(id, $scope.person, function() {
                      $rootScope.stopLoader();
                      });
                    });
                  }
                });
              },
              function(FPError) {
                console.log(FPError);
              },
              "AndroidPhoto", "androidPhoto", []);
              };
            console.log('we are down here!');
            window.androidPhoto();
          }
        }
      });
    }
  }
});

app.controller('MobileChooseGroup', function($scope, $navigate, $rootScope) {
  $scope.goToGroup = function(group) {
    $rootScope.choose_group(group.split('_')[1]);		// 
    // $rootScope.query.groups = group.split('_')[1];
    // $navigate.go('/manage-users');
  };
});

app.controller('ViewPersonsControllerMobile', function ($scope, $timeout, $rootScope, $navigate, persons, $rootScope) {
  console.log('Loading ViewPersons Controller');
  console.log($rootScope.went_back);
  if ($rootScope.went_back == false) {
    console.log('Scrlling to top');
    $timeout(function() {
      $('.mb-page .content').scrollTop(0);
    },50);
  }  
  else {
    $rootScope.went_back = false;
  }
  $('#mobile-drawers').show();
  function setSearchPhrase() {
    console.log($rootScope.groups);
    console.log('New Query');
    var group_phrase = '';
    var name_phrase = '';
    if ($rootScope.query.groups == '*') {
      $scope.searchPhrase = 'All groups';
    }
    else {
      group_phrase = ' in ' + $rootScope.groups[$rootScope.query.groups].name;
    }
    if ($rootScope.query.name == '') {
      name_phrase = 'ALL';
    }
    else {
      name_phrase = $rootScope.query.name;
    }
    $scope.searchPhrase = (name_phrase + group_phrase).toUpperCase();
  };
  $rootScope.$watch('query.groups', function(newVal) {
    setSearchPhrase();
  });
  $rootScope.$watch('query.name', function(newVal) {
    setSearchPhrase();
  });
  $scope.persons = persons;
  $scope.navigate = $navigate;
  $scope.panel = function() {
    console.log('Expanding');
    $('.mobile-list').scrollTop(0);
    if( $rootScope.snapper.state().state=="right" ){
      $rootScope.snapper.close();
    } else {
      $rootScope.query.name = '';
      $rootScope.snapper.expand('right');
    }
  }

  $scope.nameSort = function(person) {
    // console.log('Sorting!');
    return person.basic_fields.name_last;
  }

  $scope.tap = function(personId) {
    // console.log('Tapped ' + personId);
    $navigate.go('/person/' + personId);
  }

  $scope.search = function (item) {
    if (item.account.status == 'inactive') {
      return false;
    }
    // console.log($rootScope.query);
    if (item.basic_fields.name_first == undefined) {                    
      item.basic_fields.name_first = '';
    }
    if (item.basic_fields.name_last == undefined) {
      item.basic_fields.name_last = '';
    }
    if (item.basic_fields.nickname != undefined && item.basic_fields.nickname != '') {
      item.basic_fields.name_first = item.basic_fields.nickname;
    }
    // Do not show Graduates
    if (item.array_fields != undefined) {
      for (var i in item.array_fields.groups) {
        if (i == 'group_neyps98uxr') {
          return false;
        }
      }
    }
    else {
      return false;
    }
    if ($rootScope.query.name == "" && $rootScope.query.person_type == "0" && $rootScope.query.groups == "*") {
      // No Query
      return true;
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type == "0" && $rootScope.query.groups == "*") {
      // Name only query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        // Look for name
        return true;
      }
    }
    if ($rootScope.query.name == "" && $rootScope.query.person_type != "0" && $rootScope.query.groups == "*") {
      // Person Type only query
      if (item.basic_fields.person_type == $rootScope.query.person_type) {
        return true;
      }
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type != "0" && $rootScope.query.groups == "*") {
      // Double query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        if (item.basic_fields.person_type == $rootScope.query.person_type) {
          return true;
        }
      }
    }
    // Queries that inlude a GROUP name
    if ($rootScope.query.name == "" && $rootScope.query.person_type == "0" && $rootScope.query.groups != "*") {
      // Group only Query
      if (item.array_fields != undefined) {
        for (var i in item.array_fields.groups) {
          if (i == 'group_' + $rootScope.query.groups) {
            return true;
          }
        }
      }
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type == "0" && $rootScope.query.groups != "*") {
      // Name only query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        // Look for name
        if (item.array_fields != undefined) {
          for (var i in item.array_fields.groups) {
            if (i == 'group_' + $rootScope.query.groups) {
              return true;
            }
          }
        }
      }
    }
    if ($rootScope.query.name == "" && $rootScope.query.person_type != "0" && $rootScope.query.groups != "*") {
      // Person Type only query
      if (item.basic_fields.person_type == $rootScope.query.person_type) {
        if (item.array_fields != undefined) {
          for (var i in item.array_fields.groups) {
            if (i == 'group_' + $rootScope.query.groups) {
              return true;
            }
          }
        }
      }
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type != "0" && $rootScope.query.groups != "*") {
      // Double query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        if (item.basic_fields.person_type == $rootScope.query.person_type) {
          if (item.array_fields != undefined) {
            for (var i in item.array_fields.groups) {
              if (i== 'group_' + $rootScope.query.groups) {
                return true;
              }
            }
          }
        }
      }
    }


    return false;
  };

});

app.controller('FirebasePersonTest', function ($scope, $routeParams, person) {
  person = person.val();
  $scope.text = $routeParams.personId;
  $scope.person = person;
});

app.controller('ManageUsersController', function (pictabuzzService, $scope, $http, persons, $rootScope, Configs) {
  $scope.groups = [];

  $scope.persons = [];
  $scope.person_types = $rootScope.tax.person_types;
  $scope.person_types[0] = 'All Person Types';

  $scope.groups[0] = {
    name: 'All Groups',
    id: '*'
  };
  var groups = $rootScope.tax.groups;
  for (var g in groups) {
    $scope.groups[parseInt(groups[g].order)] = {
      name: groups[g].name,
      id: g
    };
  }

  $scope.chooseGroup = function(id,name) {
    $rootScope.query.groups = id;
    $rootScope.current_search = name;
  }

  if ($rootScope.query.groups == 'All Groups') {
    $scope.chooseGroup('*', 'All Groups');
  }
  var fb_persons = persons;
  for (var p in fb_persons) {
    var person = fb_persons[p];
    person.id = p;
    if (person.basic_fields.photo_file_thumb == undefined) {
      person.basic_fields.photo_file_thumb = Configs.photoThumb;
    }
    if (person.account == undefined) {
      person.account = {};
    }
    if (person.account.status == 'active' || $rootScope.user.auth.role == 'admin') {
      $scope.persons.push(person);
    }
  }

  $scope.search = function (item) {
    if (item.basic_fields.name_first == undefined) {			
      item.basic_fields.name_first = '';
    }
    if (item.basic_fields.name_last == undefined) {
      item.basic_fields.name_last = '';
    }

    if ($rootScope.query.name == "" && $rootScope.query.person_type == "0" && $rootScope.query.groups == "*") {
      // No Query
      return true;
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type == "0" && $rootScope.query.groups == "*") {
      // Name only query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        // Look for name
        return true;
      }
    }
    if ($rootScope.query.name == "" && $rootScope.query.person_type != "0" && $rootScope.query.groups == "*") {
      // Person Type only query
      if (item.basic_fields.person_type == $rootScope.query.person_type) {
        return true;
      }
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type != "0" && $rootScope.query.groups == "*") {
      // Double query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        if (item.basic_fields.person_type == $rootScope.query.person_type) {
          return true;
        }
      }
    }
    // Queries that inlude a GROUP name
    if ($rootScope.query.name == "" && $rootScope.query.person_type == "0" && $rootScope.query.groups != "*") {
      // Group only Query
      if (item.array_fields != undefined) {
        for (var i in item.array_fields.groups) {
          if (i == 'group_' + $rootScope.query.groups) {
            return true;
          }
        }
      }
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type == "0" && $rootScope.query.groups != "*") {
      // Name only query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        // Look for name
        if (item.array_fields != undefined) {
          for (var i in item.array_fields.groups) {
            if (i == 'group_' +  $rootScope.query.groups) {
              return true;
            }
          }
        }
      }
    }
    if ($rootScope.query.name == "" && $rootScope.query.person_type != "0" && $rootScope.query.groups != "*") {
      // Person Type only query
      if (item.basic_fields.person_type == $rootScope.query.person_type) {
        if (item.array_fields != undefined) {
          for (var i in item.array_fields.groups) {
            if (i == 'group_' +  $rootScope.query.groups) {
              return true;
            }
          }
        }
      }
    }
    if ($rootScope.query.name != "" && $rootScope.query.person_type != "0" && $rootScope.query.groups != "*") {
      // Double query
      if ((item.basic_fields.name_first.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1 || item.basic_fields.name_last.toLowerCase().indexOf($rootScope.query.name.toLowerCase()) != -1)) {
        if (item.basic_fields.person_type == $rootScope.query.person_type) {
          if (item.array_fields != undefined) {
            for (var i in item.array_fields.groups) {
              if (i == 'group_' +  $rootScope.query.groups) {
                return true;
              }
            }
          }
        }
      }
    }

    return false;
  };

});

app.controller('EditGroupsController', function ($scope, $http, pictabuzzService, $rootScope, $timeout) {
  $scope.groups_clone = [];
  $scope.num_groups = 0;
  pictabuzzService.getClasses(function(ss){
    var groups = ss.val();
    for (var group in groups) {
      if ($scope.groups_clone[parseInt(groups[group].order)] == undefined) {
        $scope.groups_clone[parseInt(groups[group].order)] = {
          id: group,
          order: groups[group].order,
          name: groups[group].name
        };
      }
      else {
        $scope.groups_clone[parseInt(groups[group].order) + 30] = {
          id: group,
          order: groups[group].order,
          name: groups[group].name
        };
      }
      $scope.num_groups++;

    }
    $scope.new_group = {
      order: String(++$scope.num_groups),
      name: ''
    };
  });

  $scope.submit = function() {
    var save_groups = {}
    for (var group in $scope.groups_clone) {
      save_groups[$scope.groups_clone[group].id] = {
        name: $scope.groups_clone[group].name,
        order: $scope.groups_clone[group].order
      };
    }
    if ($scope.new_group.name != '') {
      save_groups[Math.random().toString(36).substring(7)] = $scope.new_group;
    }
    $rootScope.startLoader();
    pictabuzzService.saveClasses(save_groups, function(res){
      if ($.browser.mobile != true) {
        alertify.success('Groups have been saved');
      }
      if ($scope.new_group.name != '') {
        $scope.new_group = {
          order: String(++$scope.num_groups),
          name: ''
        }
      }
      $rootScope.stopLoader();
    });
  }


});

app.controller('AddUserController', function ($scope, $http, pictabuzzService, $rootScope, $timeout) {
  $(":required")
  .closest(".control-group")
  .children("label")
  .append("<i class='icon-asterisk' style='font-size: 9px; color: red; position: relative; top: -9px; left: 2px;'></i> ");
  $scope.person = { basic_fields: {}};
  $scope.submit = function() {
    if ($scope.username == undefined || $scope.person.basic_fields.name_first == undefined || $scope.person.basic_fields.name_last == undefined || $scope.person.basic_fields.person_type == undefined) {
    }
    else {
      $rootScope.startLoader();
      if ($scope.password != '') {
        var data = {
          username: $scope.username,
          password: $scope.password,
          email: $scope.email,
        };
      } else {
        var data = {
          username: $scope.username,
          email: $scope.email,
        };
      }

      if ($scope.person.basic_fields == undefined) {
        $scope.person.basic_fields = {};
      }
      $scope.person.account = {};
      $scope.person.account.username = $scope.username;
      $scope.person.account.type = 'full';
      $scope.person.account.status = 'active';
      pictabuzzService.checkUserId($scope.username, function(result) {
        if (result) {
          pictabuzzService.createPerson($scope.person, function(id){
            $rootScope.stopLoader();
            data.id = id;
            data.fullname = $scope.person.basic_fields.name_first + ' ' + $scope.person.basic_fields.name_last;
            pictabuzzService.registerUser( data, function( result ) {
            },
            function( message, level ) {
            }
            );
            if ($.browser.mobile != true) {
              alertify.success('Person has been added');
              window.location = '/#/person/' + id;
            }
          }, function(error){
          });
        }
        else {
          alert('That person already exists');
          $rootScope.stopLoader();
        } 
      });

    }
  }

  $scope.meta = {
    legend: 'Create a New User',
    button_text: 'Add User'
  };

});

app.controller('ImportStudentsController', function ($rootScope, $scope, $routeParams, pictabuzzService, $timeout, $q, $http, Configs) {
  $scope.uploadCSV = true;
  $scope.mapCSV = false;
  $scope.uploadSamplePhoto = false;
  $scope.generatePhotoFilter = false;
  $scope.uploadAllPhotos = false;
  $scope.csv;
  $scope.csvKeys = null;
  $scope.selector = {};
  $scope.fields= [
    'user_id',
    'name_first',
    'name_last',
    'nickname',
    'cell_phone',
    'home_email',
    'home_address',
    'home_city',
    'home_phone',
    'home_state',
    'home_zip',
    'work_address',
    'work_city',
    'work_email',
    'work_employer',
    'work_phone',
    'work_state',
    'work_zip',
    'groups',
  ];
  $scope.searchField = {};
  $scope.match = false;
  var persons = [];
  var checkMatch = function(index, photoName, search, cb) {
    if (search != undefined) {
      if (search.two == null) {
        var two = '';
      }
      else {
        var two = persons[index][search.two].toLowerCase();
      }
      if (search.connector == null) {
        search.connector = '';
      }
      if (search.one == null) {
        var one = '';
      }
      else {
        var one = persons[index][search.one].toLowerCase();
      }
      console.log(photoName.toLowerCase());
      console.log(search);
      console.log(one);
      console.log(search.connector);
      console.log(two);
      var searchExpression = (one + search.connector + two).toLowerCase();
      if (photoName.toLowerCase().search(searchExpression) != -1) {
        cb(true)
      }
      else {
        cb(false);
      }
    }
  };
  $scope.$watchCollection('searchField', function(search) {
    if (search != undefined && $scope.generatePhotoFilter) {
      if (search.two == null) {
        var two = '';
      }
      else {
        var two = persons[0][search.two];
      }
      if (search.connector == null) {
        search.connector = '';
      }
      if (search.one == null) {
        var one = '';
      }
      else {
        var one = persons[0][search.one];
      }
      $scope.searchExpression = (one + search.connector + two).toLowerCase();
      if ($scope.samplePhotoName.search($scope.searchExpression) != -1) {
        $scope.match = true;
        $scope.currentSearch = search;
      }
      else {
        $scope.match = false;
        $scope.currentSearch = null;
      }
    }
  });
  $scope.$watch('csv', function(csv) {
    if (csv != undefined) {
      if (csv.data != undefined) {
        console.log('CSV is not undefined');
        $scope.csvKeys = csv.data[0];
        $scope.csvSample = csv.data[1];
        $scope.uploadCSV = false;
        $scope.mapCSV = true;
      }
    }
  });
  $scope.$watch('samplePhoto', function(samplePhoto) {
    if (samplePhoto != undefined) {
      console.log(samplePhoto);
      $scope.samplePhotoName = samplePhoto.meta.name.toLowerCase();
      $scope.uploadSamplePhoto = false;
      $scope.generatePhotoFilter = true;
      $scope.searchField.one = 'user_id';
      $scope.searchField.two = null;
      $scope.searchField.connector = null;
    }
  });
  $scope.$watch('photos', function(photos) {
    if (photos != undefined) {
      console.log(photos);
      $scope.personImageMap = {};
      _.each(persons, function(person, personIndex) {
        checkPhotos(photos, person, personIndex);
      });
      $scope.uploadAllPhotos = false;
      $scope.finalEdit = true;
    }
  });
  var checkPhotos = function(photos, person, personIndex) {
    _.each(photos, function(photo) {
      checkMatch(personIndex, photo.meta.name, $scope.currentSearch, function(result) {
        if (result) {
          alertify.success(photo.meta.name);
          personImageMap(persons[personsIndex].user_id) = photo;
        }
      });
    });
  }
  $scope.submitMap = function() {
    // Debugging:
    var map = {};
    _.each($scope.selector, function (csvIndex, pbFieldName) {
      map[pbFieldName] = $scope.csvKeys[csvIndex];   
    });
    // Code
    map = $scope.selector;
    map = _.invert(map);
    console.log(map);
    $scope.csv.data.shift();
    _.each($scope.csv.data, function(row) {
      var person = {};
      _.each(row, function (field, index) {
        if (map[index] != undefined) {
          person[map[index]] = field;  
        }
      });
      persons.push(person);
    });
    $scope.samplePerson = persons[0].name_first + ' ' + persons[0].name_last;
    $scope.persons = persons;
    $scope.mapCSV = false;
    $scope.uploadSamplePhoto = true;
  };
  
  $scope.acceptSearch = function() {
    $scope.generatePhotoFilter = false;
    $scope.uploadAllPhotos = true;
    
  }






  var importing_photos = false;
  var importing_classes = false;
  var importing_relationships = false;
  var migrate_student_groups = false;
  var add_parents = false;

  $scope.submit = function() {
    if (add_parents == true) {
      var unique_persons = {};
      var deferred = $q.defer();
      deferred.promise.then(function () { 
        _.each(unique_persons, function(person, key){
          // pictabuzzService.createPerson(person, function(new_id) {
            // 	_.each(person.array_fields.relationships, function(student_id, key) {
              // 	});
        // });
        pictabuzzService.registerParent( person, 
          function( result ) {
          },
          function( message, level ) {
          }
        );
        });
      });
      pictabuzzService.getPersons(function(ss){
        var persons = ss.val();
        var id_map = {}
        _.each(persons, function(person, id) {
          id_map[person.account.username] = id;
        });
        var unique_relationship_types = {};
        var records = JSON.parse($scope.import_json);
        _.each(records, function(record) {
          var adult_record_id = record['Record ID'];
          var adult_email = (record['IndsAdrs_1_01_Cnts_3_01_Contactnum'] != '') ? record['IndsAdrs_1_01_Cnts_3_01_Contactnum'] : ((record['IndsAdrs_1_01_Cnts_4_01_Contactnum'] != '') ? record['IndsAdrs_1_01_Cnts_4_01_Contactnum'] : 'false');
          var adult_username = (record['IndsAdrs_1_01_Cnts_3_01_Contactnum'] != '') ? record['IndsAdrs_1_01_Cnts_3_01_Contactnum'] : record['Relation record ID'] + '-parent';
          var adult_firebase_id = id_map[adult_username];
          unique_persons[adult_record_id] = {
            firebase_id: adult_firebase_id,
            record_id: adult_record_id,
            email: adult_email,
            firebase_username: adult_username
          };
          if (adult_email == 'false') {
          }
        });
        $scope.$apply(function () { 
          deferred.resolve(); 
        })
      });		
    }
    if (migrate_student_groups == true) {
      pictabuzzService.migrateStudenGroups(function(response){
      });
    }
    if (importing_relationships == true) {
      var unique_persons = {};
      var kid_relationships = {};
      var deferred = $q.defer();
      deferred.promise.then(function () { 
        _.each(unique_persons, function(person){
          // Add person to firebase
          // Get firebase ID
          pictabuzzService.createPerson(person, function(new_id) {

            // So this needs to be done atomically, so that we can add both parents to each student. Also, so that we can add parents to one another.

            _.each(person.array_fields.relationships, function(student_id, key) {
              // Load relationship-person (student)
              // Add new relationship with new parent firebase ID
              // pictabuzzService.getPerson(key, function(ss){
                // var student_person = ss.val();
                // if (student_person.array_fields.relationships == undefined) {
                  // 	student_person.array_fields.relationships = {};
                  // }
                  var new_relationship = {
                    name: person.basic_fields.name_first + ' ' + person.basic_fields.name_last,
                    person_type: 'Parent/Guardian'
                  };

                  pictabuzzService.saveRelationship(key, new_id, new_relationship, function(res){
                    // Parent now added to student. Time to add all the other people (brothers and sisters) to this student as well.
                    _.each(person.array_fields.relationships, function(sibling_object, sibling_id) {
                      pictabuzzService.getPerson(sibling_id, function(ss){
                        var sibling_object = ss.val();
                        if (sibling_id != key) {
                          // Don't add self as relationship!
                          var sib_relationship = {
                            name: sibling_object.basic_fields.name_first + ' ' + sibling_object.basic_fields.name_last,
                            person_type: 'Student'
                          };
                          pictabuzzService.saveRelationship(key, sibling_id, sib_relationship, function(res){
                          });
                        }
                      });
                    });
                  });
            // });
            });
          });

          // 
          // Add person to redis nodejs db, using email as userid.
          // Do NOT email person userid and password.
        });
      });
      pictabuzzService.getPersons(function(ss){
        var persons = ss.val();
        var id_map = {}
        _.each(persons, function(person, id) {
          id_map[person.account.username] = id;
        });
        var unique_relationship_types = {};
        var records = JSON.parse($scope.import_json);
        _.each(records, function(record) {
          var student_username = record['Relation record ID'];
          var student_firebase_id = id_map[student_username];
          var student_firebase_object = persons[student_firebase_id];
          var relationship_type = record['Reciprocal relationship'];
          unique_relationship_types[relationship_type] = true;
          // unique_persons[record['Record ID']] = record;
          //
          // Add person to Pictabuzz
          //
          // Map all school fields to pictabuzz fields
          if (unique_persons[record['Record ID']] == undefined) {
            var person = {
              account: {
                status: 'active',
                type: 'full',
                username: (record['IndsAdrs_1_01_Cnts_3_01_Contactnum'] != '') ? record['IndsAdrs_1_01_Cnts_3_01_Contactnum'] : record['Relation record ID'] + '-parent'
              },
              array_fields: {
                relationships: {	
                }
              },
              basic_fields: {
                home_address: record['Address line 1'],
                home_city: record['City'],
                home_email: record['IndsAdrs_1_01_Cnts_3_01_Contactnum'],
                home_phone: record['IndsAdrs_1_01_Cnts_1_01_Contactnum'],
                home_state: record['State/province'],
                home_zip: record['ZIP/Postcode'],
                name_first: record['First name'],
                name_last: record['Last name'],
                // work_address: record[''],
                // work_city: record[''],
                // work_email: record[''],
                // work_employer: record[''],
                work_phone: record['IndsAdrs_1_01_Cnts_2_01_Contactnum'],
                // work_state: record[''],
                // work_zip: record[''],	
                person_type: 4, // Parent
                photo_file_thumb: Configs.photoThumb
              }
            }
            person.array_fields.relationships[student_firebase_id] = {
              name: student_firebase_object.basic_fields.name_first + ' ' + student_firebase_object.basic_fields.name_last,
              person_type: 'Student'
            };
            unique_persons[record['Record ID']] = person;
          }
          else {
            unique_persons[record['Record ID']].array_fields.relationships[student_firebase_id] = {
              name: student_firebase_object.basic_fields.name_first + ' ' + student_firebase_object.basic_fields.name_last,
              person_type: 'Student'
            };
          }

        });

        $scope.$apply(function () { 
          deferred.resolve(); 
        })
      });

    }
    if (importing_photos == true) {
      var records = JSON.parse($scope.import_json);
      var unique_students = {};
      _.each(records, function(record) {
        var student_id = record['Relation record ID'];
        unique_students[record['Relation last name'].toLowerCase() + '_' + record['Relation first name'].toLowerCase()] = record;
      });
      var students_number = 0;
      var non_students_number = 0;
      $http({
        url: '/import_photos',
      method: 'GET'})
      .success(function(response, status) {
        _.each($(response).find('li a'), function(image) {
          // Go through each student record
          var name = $(image).attr('href');
          if ((name.split("_").length - 1) == 2) {
            var str = name,
            delimiter = '_',
            tokens = str.split(delimiter).slice(0,2),
            short_name = unescape(tokens.join(delimiter));
          }
          var fpfile = 'http://local.pictabuzz/import_photos/' + name;
          if (short_name != undefined) {
            if (unique_students[short_name.toLowerCase()] != undefined) {
              // Found a student, found a record, found an image

              importStudent(unique_students[short_name.toLowerCase()], short_name, name, function(result){
              });

              // Remove student added so that we an so who is left.
              delete unique_students[short_name.toLowerCase()];

            }
            else {
              // Found a photo without a record (probably a teacher, or a student's name mispelled)
            }
          }

        });
        // Students that did not have a picture.
        _.each(unique_students, function(student, key){
          importStudentNoPhoto(student, key);
        });
      });
    }
    if (importing_classes == true) {
      var class_map = {
        '3K'     :'3K',
        'JK'    :'JK',
        'K'     :'K',
        '1st Grade':'1',
        '2nd Grade':'2',
        '3rd Grade':'3',
        '4th Grade':'4',
        '5th Grade':'5',
        '6th Grade':'6'
      };
      pictabuzzService.getClasses(function(ss){
        var classes_object = ss.val();
        var new_class_map = {};
        _.each(classes_object, function(class_i, key) {
          if (class_map[class_i['name']] != undefined) {
            new_class_map[class_map[class_i['name']]] = key;
          }
        });
        var records = JSON.parse($scope.import_json);
        var unique_classes = {};
        pictabuzzService.getPersons(function(ss){
          var persons = ss.val();
          var id_map = {}
          _.each(persons, function(person, id) {
            id_map[person.account.username] = id;
          });
          _.each(records, function(record) {
            var username = record['Relation record ID'];
            var firebase_id = id_map[username];
            var firebase_object = persons[firebase_id];
            var current_grade = record['Relation current grade'];
            var firebase_grade_key = new_class_map[current_grade];
            if (firebase_object == undefined) {
            }
            else if (firebase_object['array_fields'] == undefined) {
              var group_key = "group_" + firebase_grade_key;
              firebase_object['array_fields'] = {
                'groups': {

                }
              };
              firebase_object['array_fields']['groups'][group_key] = {name: classes_object[firebase_grade_key]['name']};
            }
            pictabuzzService.savePerson(firebase_id, firebase_object, function(){}, function(){});
            // unique_classes[current_grade] = (unique_classes[current_grade] == undefined) ? 0 : unique_classes[current_grade] + 1;
          });
        });
      });
    }
  }

  function importStudentNoPhoto(record, short_name) {
    var data = {
      username: (record['Relation record ID'] != undefined) ? record['Relation record ID'] : short_name + '@default.com',
    };
    var person = {};
    person.basic_fields = {};
    person.account = {};
    person.account.username = data.username;
    person.account.type = 'restricted';
    person.account.status = 'active';
    person.basic_fields.name_first = record['Relation first name'];
    person.basic_fields.name_last = record['Relation last name'];
    person.basic_fields.name_nickname = record['Relation nickname'];
    person.basic_fields.person_type = "2";
    pictabuzzService.createPerson(person, function(id){
      // Success
      data.id = id;
      pictabuzzService.registerUser( data, 
        // Success
        function( result ) {
          // callback(result);
        },
        // Error
        function( message, level ) {
        }
      );
    },
    // Error
    function(error) {
    });
  }

  function importStudent(record, short_name, name, callback){
    if (short_name != false) {	
      var image = new Image();
      image.src = "http://local.pictabuzz/import_photos/" + name;
      getBase64Image(image, function(image_b64) {
        filepicker.store(image_b64, 
          {
            filename: name, mimetype: 'image/jpeg', location: 'S3', base64decode: true, access: 'public'
          }, 
          // Successful Store of Image
          function(FPFile) {
            if (FPFile.size > 5) {
              filepicker.convert(FPFile, {width: 400, height: 400, fit: 'crop'}, {location: "S3", access: "public"},
                // Successful Conversion of Image
                function(thumb_big){
                  filepicker.convert(FPFile, {width: 100, height: 100, fit: 'crop'}, {location: "S3", access: "public"},
                    function(thumb_small){
                      // FPFile = raw file object
                      // thumb_big = big thumbnail file object
                      // thumb_small = smal thumbnail file object

                      // Add person to Firebase, add user to Node Server
                      var data = {
                        username: (record['Relation record ID'] != undefined) ? record['Relation record ID'] : short_name + '@default.com',
                      };
                      var person = {};
                      person.basic_fields = {};
                      person.account = {};
                      person.account.username = data.username;
                      person.account.type = 'restricted';
                      person.account.status = 'active';
                      person.basic_fields.photo_file = thumb_big.key;
                      person.basic_fields.photo_file_thumb = thumb_small.key;
                      person.basic_fields.name_first = record['Relation first name'];
                      person.basic_fields.name_last = record['Relation last name'];
                      person.basic_fields.name_nickname = record['Relation nickname'];
                      person.basic_fields.person_type = "2";
                      pictabuzzService.createPerson(person, function(id){
                        // Success
                        data.id = id;
                        pictabuzzService.registerUser( data, 
                          // Success
                          function( result ) {
                            callback(result);
                          },
                          // Error
                          function( message, level ) {
                          }
                        );
                      },
                      // Error
                      function(error) {
                      });
                    }
                  );
                },
                // Error
                function(error) {
                }
              );
            }
          }, 
          // Error
          function(FPError) {
          }, 
          // Progres
          function(progress) {
          }
        );
      });
    }
  }

});

app.controller('ViewPersonController', function ($rootScope, $scope, $routeParams, person, pictabuzzService, $timeout, $q, Configs) {
  $scope.panel = function() {            
    $('.mobile-list').scrollTop(0);             
    if( $rootScope.snapper.state().state=="right" ){            
      $rootScope.snapper.close();               

    } else {            
      $rootScope.query.name = '';               
      $rootScope.snapper.expand('right');               
    }           
  }
  if ($rootScope.groups == undefined) {
    pictabuzzService.getClasses(function(ss){
      var groups = ss.val();
      _.each(groups, function(group, key) {
        if (key.search('_') != -1) {
          //  groups[key.split('_')[0]] = group;
        }
      });
      $scope.rootgroup = $rootScope.groups = groups;
    });
  }
  $scope.rootgroup = $rootScope.groups;

  $scope.password = {};
  var id = person.name();
  var user_viewing_id = $rootScope.user.auth.id;
  $scope.role = $rootScope.user.auth.role;
  $scope.person = {};
  $scope.person = person.val();
  $scope.notes = '';
  pictabuzzService.getNotes(user_viewing_id, id, function(ss){
    $scope.notes = ss.val();
  });
  $scope.takeNotes = function() {
    pictabuzzService.takeNotes(user_viewing_id, id, $scope.notes);
  }
  if ($scope.person.array_fields != undefined) {
    if ($scope.person.array_fields.relationships != undefined) {
      for (var relation_id in $scope.person.array_fields.relationships) {
      }
    }
  }
  if ($scope.person.basic_fields.photo_file == undefined) {
    $scope.person.basic_fields.photo_file = Configs.photoLarge;
  }
  if ($scope.person.account == undefined) {
    $scope.person.account = {};
  }
  if ($scope.person.account.status == undefined) {
    $scope.person.account.status = "inactive";
  }
  if ($scope.person.account.type == undefined) {
    $scope.person.account.type = 'full';
  }
  $scope.saveable = ($rootScope.user.auth.role == 'admin' || $rootScope.user.auth.role == 'admin' || ($rootScope.user.auth.id == id && $scope.person.account.type == 'full')) ? true : false;
  $scope.meta = {
    legend: 'Edit User',
    button_text: 'Save'
  };

  $scope.person_type = $rootScope.tax.person_types[$scope.person.basic_fields.person_type];
  $scope.person_self = ($rootScope.user.auth.id == $routeParams.personId) ? true : false;

  $scope.relations = {};
  if ($scope.person.array_fields != undefined) {
    if ($scope.person.array_fields.relationships != undefined) {
      for (var relation_id in $scope.person.array_fields.relationships) {
        pictabuzzService.getPerson(relation_id, function(ss) {
          $scope.relations[ss.name()] = ss.val();
          if ($scope.relations[ss.name()].basic_fields.photo_file_thumb == undefined) {           
            $scope.relations[ss.name()].basic_fields.photo_file_thumb = Configs.photoThumb;                
          }
        });
      }
    }
  }

  $scope.resetPassword = function() {
    pictabuzzService.resetPassword({
      username: $scope.person.account.username,
    }, function(result) {
      if (result == 'true') {
        // Password changed
        if ($.browser.mobile != true) {
          alertify.success('Password has been reset and an email sent to user.');
        }
      }
      else {
        alert('Error, password unchanged. Try again.');
      }
    });
  }

  $scope.deleteUser = function() {
    var yes_no = confirm('Are you sure you want to delete this user? This is unrecoverable.');
    $scope.person.account.id = id;
    if (yes_no) pictabuzzService.deleteUser($scope.person,function(res){
      if ($.browser.mobile != true) {
        alertify.success('Person has been removed');
      }
      window.location = '/#/manage-users';
    });
  }

  $scope.submit = function() {
    $rootScope.startLoader();
    var person = $rootScope.objFix($scope.person);
    pictabuzzService.savePerson(id, person, function() {
      $rootScope.stopLoader();
      if ($.browser.mobile != true) {
        alertify.success('Person has been saved');
      }
    }, function(error){
    });
    if ($scope.password.old != undefined && $scope.password.new_one != undefined && $scope.password.new_two != undefined && ($scope.password.new_one == $scope.password.new_two) && ($scope.password.new_one != '') && ($scope.password.new_two != '') && ($scope.password.old != '')) {
      pictabuzzService.changePassword({
        username: $rootScope.user.auth.email,
        old_password: $scope.password.old,
        new_password: $scope.password.new_one
      }, function(result) {
        if (result == 'true') {
          // Password changed
          if ($.browser.mobile != true) {
            alertify.success('Password has been saved.');
          }
        }
        else {
          alert('Error, password unchanged. Try again.');
        }
      });
    }
  }
  $scope.uploadConfirm = function() {
    if ($.browser.mobile != true) {
      alertify.confirm('When picking your photo, choose a clear, close-up image of YOUR FACE - not a group shot or abstract image. Make sure people can recognize you from your Pictabuzz image.', function(e) {
        if (e) {
          $scope.upload();
        }
      });
    }
    else {
      alert('When picking your photo, choose a clear, close-up image of YOUR FACE - not a group shot or abstract image. Make sure people can recognize you from your Pictabuzz image.');
    }
  }
  $scope.upload = function() {
    var promises = [$q.defer(), $q.defer()];
    var promise = $q.all([promises[0].promise, promises[1].promise]);
    promise.then(function(){
      pictabuzzService.savePerson(id, $scope.person, function() {
        $rootScope.stopLoader();
        if ($.browser.mobile != true) {
          alertify.success('Person has been saved');
        }
      }, function(error){
      });
    });
    if ($.browser.mobile == true) {
      var services = ['COMPUTER', 'FACEBOOK'];
    }
    else {
      var services = ['COMPUTER', 'FACEBOOK', 'WEBCAM'];
    }
    filepicker.pick(
      {
        mimetypes: ['image/*', 'text/plain'],
        container: 'window',
        services: services,
      },
      function(FPFile){
        filepicker.convert(FPFile, {width: 400, height: 400, fit: 'crop'}, {location: "S3", access: "public"},
          function(new_FPFile){
            $timeout(function() {
              $scope.person.basic_fields.photo_file = new_FPFile.key;
              promises[0].resolve();
            },0);
          }
        );
        filepicker.convert(FPFile, {width: 100, height: 100, fit: 'crop'}, {location: "S3", access: "public"},
          function(new_FPFile){
            $timeout(function() {
              $scope.person.basic_fields.photo_file_thumb = new_FPFile.key;
              promises[1].resolve();
            },0);
          }
        );
      },
      function(FPError){
      }
    );
  }


  $scope.addRelationship = function() {
    $scope.rel_list = [];
    $scope.person_list_names = [];
    if ($scope.rel_list.length == 0)
      $scope.rel_list.push(true);
    pictabuzzService.getPersons(function(persons) {
      persons.forEach(function(ss) {
        var person = ss.val();
        var name = ss.name();
        $scope.person_list_names.push({
          label: person.basic_fields.name_first + ' ' + person.basic_fields.name_last, 
          person_type: person.basic_fields.person_type != undefined ? person.basic_fields.person_type : 0, 
          value: name
        });
      });
    });
  }

  $scope.relationshipAdded = function(item) {
    if ($scope.person.array_fields == undefined) {
      $scope.person.array_fields = {};
    }
    if ($scope.person.array_fields.relationships == undefined) {
      $scope.person.array_fields.relationships = {};
    }
    item.person_type_value = item.person_type != 0 ? $rootScope.tax.person_types[item.person_type] : 'N/A';
    $scope.person.array_fields.relationships[item.value] = {name: item.label, person_type: item.person_type_value};
  }


  $scope.addGroup = function() {
    $scope.group_list = [];
    $scope.group_names = [];
    if ($scope.group_list.length == 0)
      $scope.group_list.push(true);
    pictabuzzService.getClasses(function(sss) {
      sss.forEach(function(ss){
        var group = ss.val();
        var name = ss.name();
        $scope.group_names.push({
          label: group.name,
          value: name
        });
      });
    });
  }

  $scope.groupAdded = function(item) {
    if ($scope.person.array_fields == undefined) {
      $scope.person.array_fields = {};
    }
    if ($scope.person.array_fields.groups == undefined) {
      $scope.person.array_fields.groups = {};
    }
    $scope.person.array_fields.groups['group_' + item.value] = {name: item.label};
  }

  $scope.addToContacts = function() {
    var person = $scope.person;
    // create a new contact object
    var contact = navigator.contacts.create();
    contact.displayName = person.basic_fields.name_first + ' ' + person.basic_fields.name_last;
    contact.nickname = person.basic_fields.nickname + ' ' + person.basic_fields.name_last;            // specify both to support all devices
    // populate some fields
    var name = new ContactName();
    name.givenName = person.basic_fields.name_first;
    name.familyName = person.basic_fields.name_last;
    contact.name = name;

    if (person.basic_fields.work_phone != undefined || person.basic_fields.home_phone != undefined) {
      var phoneNumbers = [];
      var phone_count = 0;
      if (person.basic_fields.work_phone != undefined) {
        phoneNumbers[phone_count++] = new ContactField('work', person.basic_fields.work_phone, false);
      }
      if (person.basic_fields.home_phone != undefined) {
        phoneNumbers[phone_count] = new ContactField('home', person.basic_fields.home_phone, true); // preferred number
      }
      contact.phoneNumbers = phoneNumbers;
    }

    var emails = [];
    emails[0] = new ContactField('home', person.basic_fields.home_email, true);
    emails[1] = new ContactField('work', person.basic_fields.work_email, false);
    contact.emails = emails;

    var addresses = [];
    addresses[0] = new ContactAddress(true, 'home', person.basic_fields.home_address, person.basic_fields.home_address, person.basic_fields.home_city, person.basic_fields.home_state, person.basic_fields.home_zip);
    addresses[1] = new ContactAddress(false, 'work', person.basic_fields.work_address, person.basic_fields.work_city, person.basic_fields.work_state, person.basic_fields.work_zip);
    contact.addresses = addresses;

    // save to device
    contact.save(function(){
      alert('Contact Saved');
    },function(){
      alert('Failed to Save Contact');
    });
  }

  $scope.removeGroup = function(group, groups) {
    var new_groups = {};
    for (var temp_group in $scope.person.array_fields.groups) {
      if ($scope.person.array_fields.groups[temp_group] != group) {
        new_groups[temp_group] = $scope.person.array_fields.groups[temp_group];
      }
    }
    $scope.person.array_fields.groups = new_groups;
  }

  $scope.removeRelationship = function(rel, rels) {
    var new_rels = {};
    for (var temp_rel in $scope.person.array_fields.relationships) {
      if ($scope.person.array_fields.relationships[temp_rel] != rel) {
        new_rels[temp_rel] = $scope.person.array_fields.relationships[temp_rel];
      }
    }
    $scope.person.array_fields.relationships = new_rels;
  }

  $scope.openLocation = function(place) {
    if (place == 'work') {
      var address =  $scope.person.basic_fields.work_address + ',' + $scope.person.basic_fields.work_city + ',' +   $scope.person.basic_fields.work_state + ',' +  $scope.person.basic_fields.work_zip;
      window.open('http://maps.google.com/maps?q=' + encodeURIComponent(address), '_blank' );
    }
    else {
      var address =  $scope.person.basic_fields.home_address + ',' + $scope.person.basic_fields.home_city + ',' +   $scope.person.basic_fields.home_state + ',' +  $scope.person.basic_fields.home_zip;
      window.open('http://maps.google.com/maps?q=' + encodeURIComponent(address) , '_blank' );
    }
  }

});

app.controller('LoginController', function ($scope, $rootScope, $http, pictabuzzService, $location) {
  $scope.location = $location;
  $scope.submit = function() {
    var data = {
      username: $scope.username,
      password: $scope.password,
    }
    pictabuzzService.loginUser( data, function( result ) {
      if (result.error == undefined) {
        $rootScope.user = result;
        $rootScope.role = result.auth.role;
        if ($.browser.mobile != true) {
          alertify.success('Login successful.');
        }
        window.location = '/#/person/' + result.auth.id;
      }
    },
    function( message, level ) {
    }
    );
  }

});
app.controller('PasswordResetRequestController', function ($scope, $rootScope, $http, pictabuzzService) {
  $scope.submit = function() {
    var data = {
      fullname: $scope.fullname,
      email: $scope.email,
    }
    pictabuzzService.passwordResetRequest( data, function( result ) {
      alertify.alert('An email with instructions has been sent to ' + $scope.email);
      window.location = '/#/login/';
    });
  }

});
