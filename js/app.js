﻿var phonegap_check = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;

  if ( phonegap_check || window.shim ) {
    var app = angular.module('pictabuzzApp', ['ngSanitize','ajoslin.mobile-navigate']);
    if (phonegap_check) {
      var alertOver = function(){};
      window.alert = function(msg) {
        navigator.notification.alert(
          msg,
          alertOver,
          'Pictabuzz',
          'Continue'
        );
      }

    }
    else {
      navigator = {
        splashscreen: {
            hide: function(){
              console.log('hiding splashscreen');
            }
        }
      }
    }

    app.directive('ngBlur', ['$parse', function($parse) {
      return function(scope, element, attr) {
        var fn = $parse(attr['ngBlur']);
        element.bind('blur', function(event) {
          scope.$apply(function() {
            fn(scope, {$event:event});
          });
        });
      }
    }]);

    app.config(function ($compileProvider){
      $compileProvider.urlSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
    });

    app.config(function ($routeProvider) {
      $routeProvider
      .when('/', {
        controller: 'HomeControllerMobile',
        templateUrl: 'app/js/partials-mobile/main.html',
        transition: "none",
        resolve: {
          user: function($q, pictabuzzService, $rootScope, $navigate) {
            var deferred = $q.defer();
            var successCb = function(result) {
              if (result.error == undefined) {
                $rootScope.user = result;
                $rootScope.role = result.auth.role;
                $navigate.go('/manage-users');
              }
              else {
                deferred.resolve();
              }
            }
            pictabuzzService.loginUser(undefined, successCb);
            return deferred.promise;                                                    
          }
        }
      })                
      .when('/manage-account', {        
        controller: 'ManageAccountController',
        templateUrl: 'app/js/partials-mobile/manage-account.html',
        transition: "slide",
      })
      .when('/information', {   
        controller: 'InformationController',
        templateUrl: 'app/js/partials-mobile/information.html',
        transition: "slide",
      })
      .when('/manage-users', {
        controller: 'ViewPersonsControllerMobile',
        templateUrl: 'app/js/partials-mobile/view-persons.html',
        transition: "none",
        resolve: {
          persons: function($q, pictabuzzService, Configs) {
            var deferred = $q.defer();
            var successCb = function(result) {
              if (result) {
                var persons = [];
                for (var person in result) {
                  var id = person;
                  var person = result[person];
                  person.id = id;
                  if (person.basic_fields.photo_file_thumb == undefined) {
                    person.basic_fields.photo_file_thumb = Configs.photoThumb;
                  }
                  persons.push(person);
                }
                navigator.splashscreen.hide();
                deferred.resolve(persons);
              }
              else {
                deferred.resolve({});
              }
            }
            pictabuzzService.getPersons(successCb);
            return deferred.promise;
          }
        }
      })                        
      .when('/person/:personId', {
        controller: 'ViewPersonController',
        templateUrl: 'app/js/partials-mobile/view-person.html',
        transition: "slide",
        resolve: {
          person: function($q, $route, pictabuzzService) {
            var nid = $route.current.params.personId;
            var deferred = $q.defer();
            var successCb = function(result) {
              if (result) {
                deferred.resolve(result);
              }
              else {
                deferred.reject();
              }
            }
            pictabuzzService.getPerson(nid,successCb);
            return deferred.promise;
          }
        }
      });
    });

    app.controller('AppCtrl', function($scope, $navigate, Configs) {
      $scope.$navigate = $navigate;
      $scope.clientShortName = Configs.clientShortName;
    });

    app.directive('ngTap', function() {
      var isTouchDevice = !!("ontouchstart" in window);
      return function(scope, elm, attrs) {
        if (isTouchDevice) {
          var tapping = false;
          elm.bind('touchstart', function() { 
            tapping = true; 
          });
          elm.bind('touchmove', function() { 
            tapping = false; 
          });
          elm.bind('touchend', function() { 
            tapping && scope.$apply(attrs.ngTap);
          });
        } 
        else {
          elm.bind('click', function() {
            // console.log(this);
            if ($(this).hasClass('footer-item') == true) {
              $('.footer-item').removeClass('active');
              $(this).addClass('active');
            }
            scope.$apply(attrs.ngTap);
          });
        }
      };
    });
    function notificationIOS(result) {
      alert(result.alert);
    }
    function notificationAndroid(result) {
      alert(result.alert);
    }
 app.controller('MobileAppCtrl', function ($scope, $rootScope, $navigate, pictabuzzService, Configs) {
      init();
      function init() {
        // filepicker.setKey('AzyvZ38TTe2Gcoj5h35MMz');
                                $rootScope.client_name = Configs.clientShortName;
        $rootScope.env = 'mobile';
        $scope.$navigate = $navigate;
        $rootScope.$navigate = $navigate;
        $rootScope.tax = {};
        $rootScope.query = {};
        $rootScope.query.person_type = "0";
        $rootScope.query.name = "";
        $rootScope.checked_login = false;
        $rootScope.goInfo = function() {
          $navigate.go('/information');
        }
        pictabuzzService.getClasses(function(ss){
          var groups = ss.val();
          $rootScope.groups = groups;
        });
        // Spinner Stuff

        var opts = {
          lines: 11, // The number of lines to draw
          length: 11, // The length of each line
          width: 2, // The line thickness
          radius: 8, // The radius of the inner circle
          corners: 0.6, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#fff', // #rgb or #rrggbb
          speed: 1.8, // Rounds per second
          trail: 45, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: true, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: 'auto', // Top position relative to parent in px
          left: 'auto' // Left position relative to parent in px
        };

        $rootScope.snapper = new Snap({
          element: document.getElementById('mobile-content'),
        });
        $rootScope.snapper.settings({
          disable: 'left',
          addBodyClasses: false,
          hyperextensible: false,
          resistance: 0.5,
          flickThreshold: 5,
          transitionSpeed: 0.3,
          easing: 'ease',
          maxPosition: 266,
          minPosition: -266,
          tapToClose: false,
          touchToDrag: true,
          slideIntent: 40,
          minDragDistance: 5
        });
        $rootScope.spinner_target = document.getElementById('spinner');
        $rootScope.spinner_object = new Spinner(opts);


      }

      $rootScope.$on("$routeChangeStart", function (event, next, current) {
        $rootScope.startLoader();
        if ($rootScope.user == undefined) {
          pictabuzzService.loginUser(undefined, function(result) {
            if (result.error == undefined) {
              $rootScope.user = result;
              $rootScope.role = result.auth.role;
            }
            else {
              $navigate.go('/');
            }
            $rootScope.stopLoader();
          });
        }
      });
      $rootScope.$on("$routeChangeSuccess", function (event, current, previous) {
        $('#background-fader').hide();
        $rootScope.spinner_object.stop();
      });
      $rootScope.startLoader = function() {
        $('#background-fader').show();
        $rootScope.spinner_object.spin($rootScope.spinner_target);
      }
      $rootScope.stopLoader = function() {
        $('#background-fader').hide();
        $rootScope.spinner_object.stop();
      }
    });


  }
  else {
    var app = angular.module('pictabuzzApp', ['ngSanitize', 'angles']);
    app.directive('file', function(){
      return {
        scope: {
          file: '='
        },
        link: function(scope, el, attrs){
          el.bind('change', function(event){
            var getData = function(file, type) {
              var meta = file;
              var reader = new FileReader();
              if (attrs.case == 'csv') {
                reader.readAsText(file);
              }
              else {
                reader.readAsDataURL(file);
              }
              reader.onload = function(e) {
                var file = e.target.result;
                if (attrs.case == 'one-photo') {
                  scope.file.data = file;
                }
                if (attrs.case == 'csv') {
                  var data = $.csv.toArrays(file);
                  scope.file.data = data;
                }
                if (attrs.case == 'many-photos') {
                  scope.file.push({data: file, meta: meta});
                }
                scope.$apply();
              }
            };
            scope.file = {};
            var files = event.target.files;
            var file = files[0];
            scope.file.meta = file;
            if (attrs.case == 'many-photos') {
              scope.file = [];
              for (var f in files) {
                if (files[f].size != undefined) {
                  getData(files[f], 'many-photos');
                }
              }
            }
            if (attrs.case == 'one-photo') {
              getData(file, 'one-photo');
            }
            if (attrs.case == 'csv') {
              getData(file, 'csv');
            }
          });
        }
      };
    });

    app.directive('autoComplete', function($timeout) {
      return function(scope, iElement, iAttrs) {
        if (iAttrs.uiItemsType == 'relationships') {
          iElement.autocomplete({
            source: scope[iAttrs.uiItems],
            select: function(event, ui) {
              scope.relationshipAdded(ui.item);
              ui.item.value = ui.item.label;
              $timeout(function() {
                iElement.trigger('input');
                iElement.val('');
              }, 0);
            }
          });
        }
        else if (iAttrs.uiItemsType == 'groups') {
          iElement.autocomplete({
            source: scope[iAttrs.uiItems],
            select: function(event, ui) {
              scope.groupAdded(ui.item);
              ui.item.value = ui.item.label;
              $timeout(function() {
                iElement.trigger('input');
                iElement.val('');
              }, 0);
            }
          });
        }
      };
    });

    app.filter('toArray', function() { return function(obj) {
      if (!(obj instanceof Object)) return obj;
      return _.map(obj, function(val, key) {
        return Object.defineProperty(val, '$key', {__proto__: null, value: key});
      });
    }});

    app.filter('myCustomFilter', function() {
      return function(input, output) {
        return input;
      }
    });

    //This configures the routes and associates each route with a view and a controller
    app.config(function ($routeProvider) {
      $routeProvider
      //Define a route that has a route parameter in it (:customerID)
      .when('/',
        {
          controller: 'HomeController',
          templateUrl: '/app/js/partials/empty.html',
          pageTitle: 'Home',
        })
        .when('/view-reports',
          {
            controller: 'LogController',
            templateUrl: '/app/js/partials/log.html',
            resolve: {
              reports: function($q, pictabuzzService) {
                var deferred = $q.defer();
                var successCb = function(result) {
                  if (result) {
                    deferred.resolve(result);
                  }
                  else {
                    deferred.reject();
                  }
                }
                pictabuzzService.getUserReport(successCb);
                return deferred.promise;
              }
            }
          })
        .when('/send-emails',
          {
            controller: 'SendMailCtrl',
            templateUrl: '/app/js/partials/send_mail.html'
          })
        .when('/send-notification',
          {
            controller: 'SendNotificationCtrl',
            templateUrl: '/app/js/partials/notifications.html'
          })
        .when('/make-calls',
          {
            controller: 'MakeCallsCtrl',
            templateUrl: '/app/js/partials/make-calls.html'
          })
        .when('/password-reset-request',
          {
            controller: 'PasswordResetRequestController',
            templateUrl: '/app/js/partials/password-reset-request.html'
          })
        .when('/login',
          {
            controller: 'LoginController',
            templateUrl: '/app/js/partials/login.html'
          })
          .when('/add-user',
            {
              controller: 'AddUserController',
              templateUrl: '/app/js/partials/add-user.html',
              pageTitle: 'Add a Person'
            })
            .when('/edit-groups',
              {
                controller: 'EditGroupsController',
                templateUrl: '/app/js/partials/edit-groups.html',
                pageTitle: 'Edit Groups'
              })
              .when('/import-students',
                {
                  controller: 'ImportStudentsController',
                  templateUrl: '/app/js/partials/import-students.html',
                  pageTitle: 'Import Students'
                })
                .when('/password-reset-login/:userHash',
                  {
                    controller: 'ConfirmAccountController',
                    templateUrl: '/app/js/partials/password-reset-login.html',
                    pageTitle: 'Reset Password'
                  })				
                .when('/confirm-account/:userHash',
                  {
                    controller: 'ConfirmAccountController',
                    templateUrl: '/app/js/partials/confirm-account.html',
                    pageTitle: 'Confirm Account'
                  })				
                  .when('/manage-users',
                    {
                      controller: 'ManageUsersController',
                      templateUrl: '/app/js/partials/manage-users.html',
                      pageTitle: 'Manage Users',
                      resolve: {
                        persons: function($q, pictabuzzService, Configs) {
                          var deferred = $q.defer();
                          var successCb = function(result) {
                            if (result) {
                              deferred.resolve(result);
                            }
                            else {
                              deferred.reject();
                            }
                          }
                          pictabuzzService.getPersons(successCb);
                          return deferred.promise;
                        }
                      }
                    })
                    .when('/person/:personId',
                      {
                        controller: 'ViewPersonController',
                        templateUrl: '/app/js/partials/view-person.html',
                        resolve: {
                          person: function($q, $route, pictabuzzService) {
                            var nid = $route.current.params.personId;
                            var deferred = $q.defer();
                            var successCb = function(result) {
                              if (result) {
                                deferred.resolve(result);
                              }
                              else {
                                deferred.reject();
                              }
                            }
                            pictabuzzService.getPerson(nid,successCb);
                            return deferred.promise;
                          }
                        }
                      })
                      .otherwise({
                        error: true,
                      });
    });

    function AppCtrl($scope, $rootScope, $location, $timeout, $http, pictabuzzService, Configs, $route) {
      console.log('Running AppCtrl');
      init();

      function init() {
	$rootScope.query = {};
	$rootScope.query.groups = "*";
	$rootScope.query.person_type = "0";
	$rootScope.query.name = "";
	$rootScope.current_search = 'All Groups';
        NProgress.configure({ speed: 100, trickleRate: 0.1, trickleSpeed: 200 });
        $rootScope.env = 'desktop';
        $scope.logoPath = Configs.logoPath;
        $scope.clientShortName = Configs.clientShortName;
        $scope.userUnloaded = true;
        filepicker.setKey(Configs.filepickerKey);
        $rootScope.cache = {};
        $rootScope.tax = {};
        $rootScope.loggedIn = 1;
        $rootScope.query = {};
        $rootScope.query.person_type = "0";
        $rootScope.query.name = "";
        $rootScope.query.groups = "All Groups";
        pictabuzzService.getPersonTypes(function(ss){
          var types = ss.val();
          $rootScope.tax.person_types = types;
        });

        pictabuzzService.getClasses(function(ss){
          var groups = ss.val();
          $rootScope.tax.groups = groups;
        });
      }

      $rootScope.$on("$routeChangeStart", function (event, next, current) {
        //ngProgress.color('#dbe0e4');
        NProgress.start();
        if ($rootScope.user == undefined) {
          pictabuzzService.loginUser(undefined, function(result){
            $scope.userUnloaded = false;
            if (result.error == undefined) {
              $rootScope.user = result;
              $rootScope.role = result.auth.role;
            }
            else {
              console.log($location.path());
              if ($location.path() != '' && $location.path() != '/' && $location.path().search('login') != 1 && $location.path().search('confirm-account') != 1 && $location.path().search('reset-password') != 1 && $location.path().search('password-reset-request') != 1 && $location.path().search('password-reset-login') != 1) {
                $rootScope.permission_denied = true;
              }
            }
          });
        }
      });

      $rootScope.$on("$routeChangeSuccess", function (event, current, previous) {
        NProgress.done();
        $scope.alertType = "hidden";
        $scope.newLocation = $location.path();
        $rootScope.show_view = true;
        if ($rootScope.permission_denied == true) {
          alertify.error('Please login to view this page.');
          $scope.newLocation = $location.path('login');
          $rootScope.pageTitle = '403';
          $rootScope.permission_denied = false;
          $rootScope.show_view = false;
        }
        else {
          if (!current.error) {
            $rootScope.pageTitle = current.$$route.pageTitle ? current.$$route.pageTitle : 'Pictabuzz';
          }
          else {
            alertify.error('Page not found');
            $scope.newLocation = $location.path();
            $rootScope.pageTitle = '404';
          }
        }
      });

      $rootScope.displayMessage = function(msg, type) {
        type = typeof type !== 'undefined' ? type : '';
        $scope.alertType = type;
        $scope.loadMessage = msg;
        $timeout(function() {
          $scope.alertType = "hidden";
          $scope.loadMessage = "Loading . . .";
        }, 3000);
        // $("html, body").animate({ scrollTop: 0 }, "slow");
      }
      $rootScope.startLoader = function() {
        NProgress.start();
      }
      $rootScope.stopLoader = function() {
        NProgress.done();
      }

      $rootScope.objFix = function(ngObj) {
        var output;

        output = angular.toJson(ngObj);
        output = angular.fromJson(output);

        return output;
      }

      $rootScope.logout = function() {
        if ($rootScope.env == 'mobile') {
          // check local storage for token
          window.localStorage.removeItem("pictabuzz_firebase_token");
          window.location = '/';
        }

        if ($rootScope.env == 'desktop') {
          // check cookie for token
          $.removeCookie('pictabuzz_firebase_token');
          window.location = '/';
        }
      }
    }

    $.fn.serializeObject = function()
    {
      var o = {};
      var a = this.serializeArray();
      $.each(a, function() {
        if (o[this.name] !== undefined) {
          if (!o[this.name].push) {
            o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
        } else {
          o[this.name] = this.value || '';
        }
      });
      return o;
    };

  }


  function getBase64Image(img, callback) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    // Get the data-URL formatted image
    // Firefox supports PNG and JPEG. You could check img.src to
    // guess the original format, but be aware the using "image/jpg"
    // will re-encode the image.
    var dataURL = canvas.toDataURL("image/jpg");
    var return_value = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    if (return_value.length > 100)
      callback(return_value);

  }
  // Jquery Plugin to check to see if person is using a mobile browser

  (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
