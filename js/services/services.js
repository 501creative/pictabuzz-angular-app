﻿app.factory('pictabuzzService', function($q, $http, $location, $rootScope, $timeout, Configs) {
  var cache = {};
  if ($rootScope.env == 'mobile') {
  } 
  var factory = {};
  factory.root = new Firebase(Configs.firebaseRootUrl);
  factory.db = factory.root.child('users');
  factory.tax = factory.root.child('taxonomy');
  factory.log = factory.root.child('log');
  factory.user_notes = factory.root.child('user_notes');

  if ( phonegap_check) {
    factory.rest_url = Configs.restUrl;
  }
  else {
    factory.rest_url = '' // Cross-domain cannot be controlled with shim and local. Need to route /rest to pb.501clients3.com for now.
  }

  factory.getNames = function(cb) {
    factory.db.once('value', function(ss) {
      var users = ss.val();
      var names = {};
      for (var i in users) {
        names[i] = users[i].basic_fields.name_first + ' ' + users[i].basic_fields.name_last;
      }
      $timeout(function(){
        cb(names);                  
      },0);	
    });
  }
  factory.getLogs = function(cb) {
    factory.log.once('value', function(ss) {
      $timeout(function(){
        cb(ss.val());                  
      },0);	
    });
  }
  factory.getClasses = function(successCb, errorCb) {
    factory.tax.child('grades').once('value', function(ss) {
      $timeout(function(){
        successCb(ss);                  
      },0);	
    });
  }

  factory.saveClasses = function(data, cb) {
    factory.tax.child('grades').set(objFix(data), function(result){
      cb(result);
    });
  }

  factory.getPersonTypes = function(successCb, errorCb) {
    factory.tax.child('person_types').once('value', function(ss) {
      $timeout(function(){
        successCb(ss);                  
      },0);	
    });
  }

  factory.getNotes = function(owner_id, person_id, successCb) {
    factory.user_notes.child(owner_id).child(person_id).on('value', function(ss){
      $timeout(function(){
        successCb(ss);
      },0);
    });
  }

  factory.takeNotes = function(owner_id, person_id, value) {
    factory.user_notes.child(owner_id).child(person_id).set(value, function(result){
    });
  }

  factory.getPersons = function (successCb, errorCb) {
    if (cache.getPersons == undefined) {
      factory.db.once('value', function(ss) {
        // console.log(ss);
        cache.getPersons = ss.val();
        $timeout(function(){
          successCb(ss.val());                  
        },0); 
      }, 
      function(error){
        console.log(error);
        var empty = [];
        if ($rootScope.env == 'mobile') {
          window.location.reload(true);
        }
        $rootScope.permission_denied = true;
        $timeout(function(){
          successCb(false)              
        },0);
      }
      );
    }
    else {
      $timeout(function(){
        successCb(cache.getPersons);                  
      },500); 
    }
  };

  factory.getPerson = function (nid, successCb) {
    factory.db.child(nid).once('value', function(ss){
      $timeout(function(){
        successCb(ss);                  
      },0);
    });
  };

  factory.createPerson = function (person, successCb, errorCb) {
    var ref = factory.db.push();
    ref.set(person, function(error){
      if (error) errorCb(error);
      else {
        $timeout(function() {
          successCb(ref.name());
          factory.log.push({
            'message': 'New Person Created',
            'data': ref.name(),
            'timestamp': new Date().getTime()
          });
        },0);
      }
    });
  };

  factory.savePerson = function (id, person, successCb, errorCb) {
    factory.db.child(id).set(objFix(person), function(error){
      if (error) errorCb(error);
      else successCb();
    });
    factory.log.push({
      'message': 'Person Saved',
      'data': id,
      'timestamp': new Date().getTime()
    });
  };

  factory.saveRelationship = function (id, new_id, new_relationship, successCb, errorCb) {
    factory.db.child(id).child('array_fields/relationships').child(new_id).set(objFix(new_relationship), function(error){
      if (error) errorCb(error);
      else successCb();
    });
  };

  factory.loginUser = function(data, successCb, errorCb) {
    // If no id and password are provided
    if (data == undefined) {
      if ($rootScope.env == 'mobile') {
        // check local storage for token
        var token = window.localStorage.getItem("pictabuzz_firebase_token");
      }

      if ($rootScope.env == 'desktop') {
        // check cookie for token
        var token = $.cookie('pictabuzz_firebase_token');
      }
      if (token != undefined) {
        factory.root.auth(token, function(error, result) {
          var output = {};
          if(error) {
            output.error = "Login Failed! " + error;
          } else {
            output.auth = result.auth;
            factory.log.push({
              'message': 'Person logged in automatically',
              'type': $rootScope.env,
              'data': output.auth.id,
              'timestamp': new Date().getTime()
            });
          }
          successCb(output);
          factory.registerPushToken(result.auth, function() {
          });
        });
      }
      else {
        successCb({error: true});
      }
    }
    else {
      window.localStorage.setItem("pictabuzz_userid", data.username);
      // Logging in with id and password
      $http({
        method: 'POST',
        data: data,
      url: factory.rest_url + '/rest/login' }).
      success(function(token, status) {
        if (token != 'false') {
          var better_token = JSON.parse(token);
          //Log me in
          factory.root.auth(better_token, function(error, result) {
            var output = {};
            if(error) {
              output.error = "Login Failed! " + error;
            } else {
              if ($rootScope.env == 'mobile') {
                // check local storage for token
                window.localStorage.setItem("pictabuzz_firebase_token", better_token);
              }
              if ($rootScope.env == 'desktop') {
                // check cookie for token
                $.cookie('pictabuzz_firebase_token', better_token);
              }
              output.auth = result.auth;
              output.date = 'Auth expires at: ' + new Date(result.expires * 1000);
            }
            successCb(output);
            factory.registerPushToken(result.auth, function() {
            });
            factory.log.push({
              'message': 'Person logged in',
              'type': $rootScope.env,
              'data': output.auth.id,
              'timestamp': new Date().getTime()
            });
          });
        }
        else {
          if ($rootScope.env == 'mobile') {
            // check local storage for token
            $rootScope.stopLoader();
          }
          alert('Wrong id or password. Try again.');
        }
      }).
      error(function(obj, status) {
      });
    }	
  }

  factory.registerUser = function(data, successCb, errorCb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/register' }).
    success(function(data, status) {
      successCb(data);
    });	
  }

  factory.registerParent = function(data, successCb, errorCb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/register-parent' }).
    success(function(data, status) {
      successCb(data);
    });	
  }

  factory.deleteUser = function(data, successCb, errorCb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/delete-user' }).
    success(function(data, status) {
      successCb(data);
    });	
  }

  factory.checkUserId = function(data, cb) {
    $http({
      method: 'POST',
      data: {userid: data},
    url: factory.rest_url + '/rest/check-user-id' }).
    success(function(data, status) {
      cb(JSON.parse(data));
    });	
  }
  factory.confirmAccount = function(data, cb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/confirm-account' }).
    success(function(data, status) {
      cb(data);
    });	
  }

  factory.changePassword = function(data, successCb, errorCb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/password' }).
    success(function(datab, status) {
      successCb(datab);
    });	
  }

  factory.sendMail = function(data, successCb, errorCb) {
    $http({
      method: 'POST',
      data: {
        type: data
      },
    url: factory.rest_url + '/rest/send-mail' }).
    success(function(datab, status) {
      successCb(datab);
    });	
  }
  factory.resetPassword = function(data, successCb, errorCb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/password-reset' }).
    success(function(datab, status) {
      successCb(datab);
    });	
  }
  factory.scheduleCalls = function(data, cb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/schedule-calls' }).
    success(function(datab, status) {
      cb(datab);
    });	
  }

  factory.uploadImage = function(data, successCb) {
    var timestamp = new Date().getTime();
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/upload_image?' + timestamp}).
    success(function(data, status) {
      successCb(data.result);
    });
  }
  factory.getUserReport = function(cb) {
    $http({
      method: 'POST',
      data: {},
    url: factory.rest_url + '/rest/user-report'}).
    success(function(data, status) {
      cb(data);
    });
  }
  factory.passwordResetRequest = function(data, cb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/password-reset-request'}).
    success(function(data, status) {
      cb(data);
    });
  }
  factory.registerPushToken = function(auth, cb) {
    if ($rootScope.env == 'mobile') {
      console.log('Registering Push Token');
      var redisUserId = auth.email;
      var pushNotification = window.plugins.pushNotification;
      if ( device.platform == 'android' || device.platform == 'Android' )
      {
        pushNotification.register(
          function(token) {
            var data = {
              platform: 'android',
              token: token,
              userId: redisUserId,
            }
            factory.sendPushToken(data);
          },
          function(result) {
          }, {
            "senderID":"replace_with_sender_id",
            "ecb":"notificationAndroid"
          });
      }
      else
      {
        pushNotification.register(
          function(token) {
            var data = {
              platform: 'IOS',
              token: token,
              userId: redisUserId,
            }
            factory.sendPushToken(data);
          },
          function(result) {
          },
          {
            "badge":"true",
            "sound":"true",
            "alert":"true",
            "ecb":"notificationIOS"
          });
      }
    }
  }

  factory.sendPushToken = function(data) {
    console.log('Sending Push Data');
    console.log(data);
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/register-push-token'}).
    success(function(data, status) {
      console.log('Sent Push Data:');
      console.log(data);
    });
  }

  factory.sendNotification = function(data, cb) {
    $http({
      method: 'POST',
      data: data,
    url: factory.rest_url + '/rest/send-notification'}).
    success(function(data, status) {
      cb(data);
    });
  }

  factory.migrateStudenGroups = function(cb) {
    factory.root.child('users').once('value', function(ss) {
      var users = ss.val();
      for (var u in users) {
        var user = users[u];
        if (user.basic_fields.person_type == 4) {
          factory.root.child('users/' + u + '/array_fields/groups/group_9mufe5yu8fr').set({name: 'Parent/Guardian'});
        }
      }
      cb();
    });
  }

  return factory;

  function objFix(ngObj) {
    var output;

    output = angular.toJson(ngObj);
    output = angular.fromJson(output);

    return output;
  }

});
