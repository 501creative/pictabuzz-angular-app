app.factory('Configs', function($q, $http, $location, $rootScope, $timeout) {
  return {
    clientShortName: 'Mirowitz',
    logoPath: '/img/mirowitz/client-logo.png',
    firebaseRootUrl: 'https://501-mirowitz-pictabuzz.firebaseio.com/',
    restUrl: 'http://mirowitz.pictabuzz.com',
    photoLarge: 'mirowitz-default.png',
    photoThumb: 'mirowitz-thumb.png', 
    baseFileUrl: 'https://pictabuzz.s3.amazonaws.com/',
    filepickerKey: 'AzyvZ38TTe2Gcoj5h35MMz',
    baseEmailAddress: 'mirowitz.org',
    subdomain: 'mirowitz',
    isIphone: true,
  };
});

